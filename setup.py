from setuptools import setup

setup(
    name='django-bleach',
    version='12.10',
    description='Whitelist-based HTML-sanitizing filters for django based on bleach.',
    long_description=open('README.rst').read(),
    author='Co-Capacity',
    author_email='django@co-capacity.org',
    url='https://bitbucket.org/cocapacity/django-bleach/',
    download_url='https://www.bitbucket.org/cocapacity/django-bleach/get/v%s.tar.gz' % get_version().replace(' ', '-'),
    license='BSD',
    packages=['dbleach'],
    include_package_data=True,
    package_data={'': ['README.rst']},
    zip_safe=False,
    install_requires=['bleach'],
    classifiers = [
      'Development Status :: 4 - Beta',
      'Environment :: Web Environment',
      'Framework :: Django',
      'Intended Audience :: Developers',
      'License :: OSI Approved :: BSD License',
      'Operating System :: OS Independent',
      'Programming Language :: Python',
      'Topic :: Software Development :: Libraries :: Python Modules',
      'Topic :: Utilities'
    ]
)