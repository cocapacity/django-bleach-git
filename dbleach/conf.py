from django.conf import settings
from appconf import AppConf

from bleach import (ALLOWED_TAGS as ALLOWED_TAGS_DEFAULT, 
                    ALLOWED_STYLES as ALLOWED_STYLES_DEFAULT, 
                    ALLOWED_ATTRIBUTES as ALLOWED_ATTRIBUTES_DEFAULT)

__all__ = ["settings", "BleachConf"]

class BleachConf(AppConf):
    """
    ``linkify()``
    -------------

    ``ALLOWED_TAGS``
    A whitelist of HTML tags. Must be a list. Defaults to
    ``bleach.ALLOWED_TAGS``.
    
    ``ALLOWED_STYLES``
    A whitelist of allowed CSS properties within a ``style`` attribute. (Note
    that ``style`` attributes are not allowed by default.) Must be a list.
    Defaults to ``bleach.ALLOWED_STYLES``.
    
    ``ALLOWED_ATTRIBUTES``
    A whitelist of HTML attributes. Either a list, in which case all attributes
    are allowed on all elements, or a dict, with tag names as keys and lists of
    allowed attributes as values ('*' is a wildcard key to allow an attribute on
    any tag). Or it is possible to pass a callable instead of a list that accepts
    name and value of attribute and returns True of False. Defaults to
    ``bleach.ALLOWED_ATTRIBUTES``.
    
    
    ``delinkify()``
    ---------------
    
    ``allow_domains``
    Allow links to the domains in this list. Set to ``None`` or an empty list to
    disallow all non-relative domains. See below for wildcards. Defaults to
    ``None``.
    
    """
    ALLOWED_TAGS = ALLOWED_TAGS_DEFAULT
    ALLOWED_STYLES = ALLOWED_STYLES_DEFAULT
    ALLOWED_ATTRIBUTES = ALLOWED_ATTRIBUTES_DEFAULT
    
    ALLOW_DOMAINS = None
    
    class Meta:
        prefix = "bleach"
