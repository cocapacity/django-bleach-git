from django import template
from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe

from bleach import linkify, delinkify, clean

from dbleach.conf import settings
from dbleach.templatetags import get_filter_kwargs


register = template.Library()


@register.filter(name='linkify')
def linkify_filter(value, args, autoescape=None):
    """
    
    """
    kwargs = get_filter_kwargs(args, ("target"), 
                               ('nofollow', 'skip_pre', 'parse_email'))
    if autoescape:
        value = conditional_escape(value)
    return mark_safe(linkify(value, **kwargs))
linkify_filter.needs_autoescape = True


@register.filter(name='delinkify')
def delinkify_filter(value, args):
    """
    
    """
    kwargs = get_filter_kwargs(args, (), ("allow_relative"))
    kwargs.update({"allow_domains": settings.BLEACH_ALLOW_DOMAINS})
    return delinkify(value, **kwargs)
delinkify_filter.is_safe = True


@register.filter(name='clean')
def clean_filter(value, args):
    kwargs = get_filter_kwargs(args, (), ("strip", "strip_comments"))
    kwargs.update(
        {"tags": settings.BLEACH_ALLOWED_TAGS,
         "attributes": settings.BLEACH_ALLOWED_ATTRIBUTES,
         "styles": settings.BLEACH_ALLOWED_STYLES}
    )
    return mark_safe(clean(value, **kwargs))


