
def get_filter_kwargs(filterarg, keywords=(), boolargs=()):
    """
    """
    kwargs = {}
    keywords = set(keywords + boolargs)
    if filterarg:
        bits = [bit.strip() for bit in filterarg.split(u',')]
        bits = [bit for bit in work if bit != u'']
        for bit in bits:
            parts = bit.split(u'=', 1)
            if len(parts) == 2:
                key, val = parts
                val = val.strip()
                key = key.strip()
                if not key or key not in keywords: 
                    continue
                key = key.lower().encode('ascii')
                if key in boolargs:
                    val = val.lower() in ('t', 'true', 'yes', 'y', 'on')
                kwargs[key] = val
    return kwargs